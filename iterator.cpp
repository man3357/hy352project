#include "functions.h"
#include <iostream>     // std::cout
#include <iterator>     // std::iterator, std::input_iterator_tag
#include <vector>


int main () {
  std::vector<int> numbers={10,20,30,40,50};
//   MyIterator from(numbers[0]);
//   MyIterator until((numbers[numbers.size()]);    

  for (auto it : numbers)
    std::cout << it << ' ';
  std::cout << '\n';

  return 0;
}