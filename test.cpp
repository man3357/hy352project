#include "./hy352_gui.h"
#include "functions.h"
#ifdef APPLE
#include <allegro5/allegro.h> //**
#endif

TO f WITH args FSTART
    SHOW: ARG(1)
    RETURN
FEND

;START_PROGRAM
    cout << "\n--------------------------------\n";
    cout << "LECTURE EXAMPLE:\n";
MAKE number = NUMBER: 21;
//define hello variable with value “hello”
MAKE hello = WORD: "hello";
//define myMoves variable contains list of turtle moves
MAKE myMoves = LIST [
                        LIST [WORD: "FORWARD", NUMBER: 100],
                        LIST [WORD: "LEFT", NUMBER: 90],
                        LIST [WORD: "FORWARD", NUMBER: 100]
                    ];
//define array variable with empty array
MAKE array = ARRAY {
                        number,
                        hello,
                        NUMBER: 12,
                        BOOLEAN: TRUE,
                        ARRAY {
                                myMoves,
                                LIST [WORD: "BACK", NUMBER: 100]
                              }
                    };
//define book variable with sentence type
MAKE book = SENTENCE (hello, WORD: "hello!");
    cout << "number\t" << number.toString() << "\n";
    cout << "word\t" << hello.toString() << "\n";
    cout << "list\t" << myMoves.toString() << "\n";
    cout << "array\t" << array.toString() << "\n";
    cout << "sentence\t" << book.toString() << "\n";

    cout <<"================================\n";
    //auto tmp1 = new Array();
    MAKE tmp2 = ARRAY {NUMBER:21,NUMBER:22,WORD:"\n",WORD:"sdsad",*(new Number(21)),WORD:"Assa"};
    //MAKE tmp2 = tmp1->arr;
    cout << "tmp2 test\n"<<tmp2.toString()<<"\n";

    cout << "================================\n";
    cout << "TEST ITEM:\n";

MAKE tmp = ITEM ({4}, array);
    cout << tmp.toString()<<"\n";
tmp = ITEM ({5,2}, array);
    cout << tmp.toString()<<"\n";
tmp = ITEM ({5,2,1}, array);
    cout << tmp.toString()<<"\n";
tmp = ITEM ({1}, array);
    cout << tmp.toString()<<"\n";
SETITEM ({3},array,WORD:"hello");
tmp = ITEM ({3}, array);
    cout << tmp.toString()<<"\n";
SETITEM ({5,2,2}, array, NUMBER:200);
tmp = ITEM ({5,2,2}, array);
    cout << tmp.toString()<<"\n";

    cout << "====================\n";
    cout << "TEST AND|OR|NOT\n";
int z = 3,i=9;
REPEAT WHILE z!=0 DO
    cout <<"shoult 1 :" << REPCOUNT << "\n";
    IF NOT(12) DO
        cout <<"error\n";
    END
    IF AND(NOT(OR(z!=3 , z==0)) , NOT(5))DO
        cout << "1\n";
        z--;
    ELSE 
        z--;
        cout << "x--;\n";
    END
END
    cout << "===========================\n";

REPEAT 5 TIMES DO
    cout<<REPCOUNT;
    REPEAT 5 TIMES DO
        IF 1==1 DO
            cout<<REPCOUNT;
            int k=0;
            REPEAT WHILE k<5 DO
                k++;
                cout << REPCOUNT;
            END
        END
    END
    cout <<endl;
END
cout <<endl;
    cout << "===========================\n";
    cout << "TEST operators\n";
    if(array!=book){
        cout << "correct\n";
    }else{
        cout << "error\n";
    }
    if(myMoves==myMoves){
        cout << "correct\n";
    }else{
        cout << "error\n";
    }
    MAKE lll = LIST [WORD: "FORWARD", NUMBER: 100];
    if(LIST [WORD: "FORWARD", NUMBER: 100]==lll){
        cout << "correct\n";
    }else{
        cout << "error <-------eprepe na einai correct\n";
    }
    if((WORD:"hello") == hello){
        cout << "correct\n";
    }else{
        cout << "error\n";
    }
    if ( (NUMBER:1) < (NUMBER:2) ){
        cout << "correct\n";
    }else{cout << "error\n";}
    if(ITEM({2},lll) <= ITEM ({5,2,2}, array)){
        cout << "correct\n";
    }else{cout << "error\n";}
    cout << "===========================\n";
    cout << "TEST operators #vol2\n";
    cout <<( (NUMBER:21) + 22 + (NUMBER:23)).toString()<< endl;
    cout <<( (NUMBER:21) - 22 - (NUMBER:23)).toString()<< endl;
    cout <<( (NUMBER:21) * 22 * (NUMBER:23)).toString()<< endl;
    cout <<( (NUMBER:21) / 22 / (NUMBER:23)).toString()<< endl;
    cout <<( (NUMBER:21) % 22 % (NUMBER:23)).toString()<< endl;
    cout << "===========================\n";
    cout << "TEST GUI:\n";
    
	set_screen_color(255, 255, 255);
	set_pen_color(0, 0, 0);
	set_pen_thickness(0.1f);

    SETPENCOLOR [NUMBER:0, NUMBER:242,NUMBER: 255]
    SETSCREENCOLOR [NUMBER:255, NUMBER:0, NUMBER:0]
    PRINT WORD: "Geia sas noobakiaaa!!!!";

    PENDOWN
    CIRCLE NUMBER:100
    SETPENSIZE NUMBER: 10
    FORWARD NUMBER:200
    CIRCLE NUMBER:100
    PENUP
    LEFT NUMBER:90
    BACK NUMBER:200
    PRINT WORD: "EDWWWW!!!!";
    cout << "===========================\n";
    cout << "TEST FUNCTION:\n";
    CALL f(LIST[NUMBER:100])
    CALL f(myMoves)

    SETXY 50,50

    MAKE vv = ARRAY SIZE:20;
    cout << vv.toString()<<endl
    MAKE a1 = NUMBER:21
    MAKE a2 = NUMBER:29
    MAKE yolo = SUM(21,a2)
    SHOW: yolo
    ASSIGN yolo = DIFFERENCE(NUMBER:21,a2)
    SHOW: yolo
    ASSIGN yolo = PRODUCT(NUMBER:21,a2)
    SHOW: yolo
    ASSIGN yolo = QUOTIENT(NUMBER:21,a2)
    SHOW: yolo
    ASSIGN yolo = MODULO(NUMBER:21,a2)
    SHOW: yolo
    ASSIGN yolo = MINUS(yolo)
    SHOW: yolo
END_PROGRAM