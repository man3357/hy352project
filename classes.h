#include <string>
#include <iostream>
#include <iterator>
using namespace std;
enum Type {N,W,B,L,A,S};
class __rip{
    public:
    enum {_IF,_REP,_MAIN} type;
    int repcount;
    __rip(){
        repcount = 0;
        type = _MAIN;
    }
    void _rep();
    void _plusplus();
    void _if();
};
class Head {
    private:
    Type type;
    double number;
    string word;
    bool boolean;
    vector<Head>* list;
    public:
    Head(){}
    Head(vector<Head> arr){
        this->list = new vector<Head>();
        this->type = A;
        for(auto i : arr){
            (this->list)->push_back(i);
        }
    }
    void setType(Type type){
        this->type = type;
    }
    Type getType(){
        return this->type;
    }
    void setNumber(double number){
        this->number = number;
    }
    double getNumber(){
        return this->number;
    }
    void setWord(string word){
        this->word = word;
    }
    string getWord(){
        return this->word;
    }
    void setBoolean(bool boolean){
        this->boolean = boolean;
    }
    bool getBoolean(){
        return this->boolean;
    }
    void setList(vector<Head>* list){
        this->list = list;
    }
    vector<Head>* getList(){
        return this->list;
    }
    string printList(vector<Head>* a);
    string toString();
    auto  begin(){return (this->list)->begin();}
    auto end(){return (this->list)->end();}
    bool operator== (Head right);
    bool operator!= (Head right);
    bool operator< (Head right);
    bool operator> (Head right);
    bool operator<= (Head right);
    bool operator>= (Head right);
    Head operator+ (Head right);
    Head operator- (Head right);
    Head operator* (Head right);
    Head operator/ (Head right);
    Head operator% (Head right);
    bool operator== (int right);
    bool operator!= (int right);
    bool operator< (int right);
    bool operator> (int right);
    bool operator<= (int right);
    bool operator>= (int right);
    Head operator+ (int right);
    Head operator- (int right);
    Head operator* (int right);
    Head operator/ (int right);
    Head operator% (int right);
};
class Number : public Head{
    public:
    Number(){setType(N);}
    Number(double number){
        setType(N);
        setNumber(number);
    }
    double get(){
        return getNumber();
    }
};
class Word : public Head{
    public:
    Word(){setType(W);}
    Word(string word){
        setType(W);
        setWord(word);
    }
    Word(const char* word){
        setType(W);
        setWord(word);
    }
    string get(){
        return getWord();
    }
};
class Bool : public Head{
    public:
    Bool(){setType(B);}
    Bool(bool boolean){
        setType(B);
        setBoolean(boolean);
    }
    bool get(){
        return getBoolean();
    }
    string getString(){
        if(getBoolean()){
            return "TRUE";
        }else{
            return "FALSE";
        }
    }
};
class List : public Head{
    public:
    List(){setType(L);}
    List(vector<Head>* list){
        setType(L);
        setList(list);
    }
    vector<Head>* get(){
        return getList();
    }

    List operator[](vector<Head>* v){
        setType(L);
        setList(v);
        //List* toret = new List(v);
        return *this;
    }
    List operator[](Head h){
        vector<Head>* v = new vector<Head>();
        v->push_back(h);
        setType(L);
        setList(v);
        //List* toret = new List(v);
        return *this;
    }
};
class Array : public Head{
    public:
    vector<Head> arr;
    Array(){setType(A);}
    vector<Head>* get(){
        return getList();
    }
    vector<Head> operator=(int size){
        vector<Head> tmp(size);
        for(int i = 0; i < size; i++){
            tmp[i] = NUMBER:0;
        }
        return tmp;
    }
};
class Sentence : public Head{
    public:
    Sentence(){setType(S);}
    Sentence(vector<Head>* list){
        setType(S);
        setList(list);
    }
    vector<Head>* get(){
        return getList();
    }
};
class myVectorSize{
    public:
    vector<Head> vv;
    myVectorSize(int size){
        vv.reserve(size);
    }
};