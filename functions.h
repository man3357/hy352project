#include <iostream>
#include <sstream>
#include <vector>
#include <cstring>
using namespace std;
#include "defines.h"
#include "classes.h"
#include "moves.h"
bool cmp(vector<Head>* a, vector<Head>* b );
Head getItem(vector<int> index , Head array);
void setItem(vector<int> index, Head array, Head item);
#include "operators.h"
#include "templates.h"

vector<__rip> __repcount;

Head getItem(vector<int> index , Head array){
    Array* a = static_cast<Array*>(&array);
    if(index.size() == 1){
        return (*(a->get()))[index[0]-1];
    }
    int i = index[0];
    vector<int> vec;
    vec.push_back(i);
    index.erase(index.begin());
    return getItem( index, getItem(vec,array));
}
void setItem(vector<int> index, Head array, Head item){
    Array* a = static_cast<Array*>(&array);
    if(index.size() == 1){
        (*(a->get()))[index[0]-1] = item;
    }else{
        int i = index[0];
        vector<int> vec;
        vec.push_back(i);
        index.erase(index.begin());
        setItem( index, getItem(vec,array), item);
    }
}

bool cmp(vector<Head>* a, vector<Head>* b ){
    if(a->size() != b->size())  return false;
    for(int i=0; i<a->size();i++){
        if( (*a)[i] != (*b)[i] )    return false;
    }
    return true;
}
Head __findArg(Head list, int i){
    return ITEM({i},list);
}
// class Head functions \/
string Head::printList(vector<Head>* a){
    string sb = "{";
    for(int i=0; i<a->size();i++){
        sb+=(*a)[i].toString();
    }
    sb+="}";
    return sb;
}
string Head::toString(){
    switch(getType()){
        case 0:
            return to_string(this->number);
        case 1:
            return this->word;
        case 2:
            if(this->boolean){
                return "TRUE";
            }else{
                return "FALSE";
            }
        case 3:
            return printList(this->list);
        case 4:
            return printList(this->list);
        case 5:
            return printList(this->list);
        default:
            return "error@\"toString\"gotWithUndefined";
    }
}
// class __rip functions \/
void __rip::_rep(){
    __rip tmp = *(new __rip());
    tmp.type = _REP;
    tmp._plusplus();
    __repcount.push_back(tmp);
}
void __rip::_plusplus(){
    if(type == _REP){
        repcount++;
    }
}
void __rip::_if(){
    __rip tmp = *(new __rip());
    tmp.type = _IF;
    tmp.repcount = LASTCOUNT.repcount;
    __repcount.push_back(tmp);
}