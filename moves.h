class __Forward{
public:
    __Forward(){}
    __Forward(double x){
        turtle_mv_forward(static_cast<float>(x));
    }
    __Forward(Head x){
        turtle_mv_forward(static_cast<float>(x.getNumber()));
    }
};
class __Back{
public:
    __Back(){}
    __Back(int x){
        turtle_mv_backward(static_cast<float>(x));
    }
    __Back(Head x){
        turtle_mv_backward(static_cast<float>(x.getNumber()));
    }
};
class __Left{
public:
    __Left(){}
    __Left(int x){
        turtle_rotate(-x);
    }
    __Left(Head x){
        turtle_rotate(-static_cast<int>(x.getNumber()));
    }
};
class __Right{
public:
    __Right(){}
    __Right(int x){
        turtle_rotate(x);
    }
    __Right(Head x){
        turtle_rotate(static_cast<int>(x.getNumber()));
    }
};
class __Setxy{
    int x,y;
    public:
    __Setxy(){}
    __Setxy(int x){
        this->x = x;
    }
    __Setxy(Head x){
        this->x = static_cast<int>(x.getNumber());
    }
    void setY(int y){
        this->y = y;
    }
    void setY(Head y){
        this->y = static_cast<int>(y.getNumber());
    }
    void move(){
        turtle_go_to_position(this->x,this->y);
    }
};
class __Circle{
public:
    __Circle(){}
    __Circle(int x){
        turtle_draw_circle(static_cast<unsigned>(x));
    }
    __Circle(Head x){
        turtle_draw_circle(static_cast<unsigned>(x.getNumber()));
    }
};
class __Print{
public:
    __Print(){}
    __Print(string x){
        char *y = new char[x.length() + 1];
        strcpy(y, x.c_str());
        turtle_draw_label(y);
        delete[] y;
    }
    __Print(Head x){
        char *y = new char[x.getWord().length() + 1];
        strcpy(y, x.getWord().c_str());
        turtle_draw_label(y);
        delete[] y;
    }
};
class __Show{
public:
    __Show(){}
    __Show(char* x){        
        cout << x <<endl;
    }
    __Show(string x){
        cout << x <<endl;
    }
    __Show(Head x){
        cout << x.toString() <<endl;
    }
};
class __Setpencolor{
public:
    __Setpencolor(){}
    __Setpencolor(Head x){
        if(x.getList()->size() != 3)    exit(0);
        unsigned r = ( *x.getList() )[0].getNumber();
        unsigned g = ( *x.getList() )[1].getNumber();
        unsigned b = ( *x.getList() )[2].getNumber();
        set_pen_color(r,g,b);
    }
};
class __Setscreencolor{
public:
    __Setscreencolor(){}
    __Setscreencolor(Head x){
        if(x.getList()->size() != 3)    exit(0);
        unsigned r = ( *x.getList() )[0].getNumber();
        unsigned g = ( *x.getList() )[1].getNumber();
        unsigned b = ( *x.getList() )[2].getNumber();
        set_screen_color(r,g,b);
    }
};
class __Setpensize{
public:
    __Setpensize(){}
    __Setpensize(float x){
        set_pen_thickness(x);
    }
    __Setpensize(Head x){
        set_pen_thickness(static_cast<float>(x.getNumber()));
    }
};