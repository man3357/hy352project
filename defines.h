#define START_PROGRAM ;int main(){ init_GUI(); __repcount.push_back( *(new __rip()) );
#define END_PROGRAM ; destroy_GUI(); return 0;}

#define TRUE true
#define FALSE false

#define MAKE ;Head
#define NUMBER *(new Number()) = 0?-1
#define WORD *(new Word()) = false?NULL
#define BOOLEAN *(new Bool()) = 0?-1
#define LIST (*(new List()))
#define ARRAY (new Array())->arr = 
#define SIZE *(new Array())= 0?-1
#define SENTENCE (*(new Sentence())) = 

#define ITEM getItem
#define SETITEM ;setItem
#define ASSIGN ;

#define OR(x,...) orr(x,##__VA_ARGS__ )
#define AND(x,...) andd(x,##__VA_ARGS__ )
#define NOT(x) nott(x)

#define SUM(x,...) summ(x,##__VA_ARGS__ )
#define DIFFERENCE(x,...) diff(x,##__VA_ARGS__ )
#define PRODUCT(x,...) prod(x,##__VA_ARGS__ )
#define MINUS(x) minuss(x)
#define QUOTIENT(x,...) quo(x,##__VA_ARGS__)
#define MODULO(x,...) modulo(x,##__VA_ARGS__)

#define REPCOUNT __repcount[__repcount.size()-1].repcount
#define LASTCOUNT __repcount[__repcount.size()-1]

#define IF ;LASTCOUNT._if(); if(
#define ELIF ;}else if(
#define ELSE ;}else{
#define REPEAT ;LASTCOUNT._rep(); for(int i=0;i<
#define TIMES ;i++
#define WHILE 0 ;){} while(
#define FOREACH ;LASTCOUNT._rep(); for(auto __elem:
#define ELEM __elem
#define DO ){
#define END ;LASTCOUNT._plusplus();} __repcount.pop_back();

//#define AND(a,b,...) new vector<Head

#define CENTER ;turtle_go_to_center();
#define FORWARD ;__Forward()=
#define BACK ;__Back()=
#define LEFT ;__Left()=
#define RIGHT ;__Right()=
#define CIRCLE ;__Circle()=

#define SETXY ;__Setxy()=

#define SETPENCOLOR ;__Setpencolor()= LIST
#define SETSCREENCOLOR ;__Setscreencolor()= LIST
#define SETPENSIZE ;__Setpensize()=
#define PENUP ;pen_up();
#define PENDOWN ;pen_down();
#define PRINT ;__Print()=
#define SHOW ;__Show()= false? *(new Head())

#define TO ;void 
#define WITH ( Head __args, int
#define FSTART  = 0){
#define FEND ;}
#define RETURN ;return
#define CALL ;
#define ARG(i) __findArg(__args,i)