#include "functions.h"

START_PROGRAM
MAKE array = ARRAY {WORD:"test"}
MAKE tmp = array
REPEAT 5 TIMES DO
    SETITEM({1},tmp,ARRAY{ NUMBER:REPCOUNT });
    tmp = ITEM({1},tmp);
    cout << array.toString() << endl;
END
END_PROGRAM