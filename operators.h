vector<Head>* operator,(Head a, Head b){
    vector<Head>* v = new vector<Head>();
    v->push_back(a);
    v->push_back(b);
    return v;
}
vector<Head>* operator,(vector<Head>* a, Head b){
    a->push_back(b);
    return a;
}
__Setxy operator,(__Setxy x , int y){
    x.setY(y);
    x.move();
}
__Setxy operator,(__Setxy x , Head y){
    x.setY(y);
    x.move();
}
//operators for Head,Head \/
bool Head::operator== (Head right){
    if(this->type != right.getType()){
        return false;
    }
    switch(this->type){
        case 0:
            return this->number == right.getNumber();
        case 1:
            return this->word == right.getWord();
        case 2:
            return this->boolean == right.getBoolean();
        case 3:
            return cmp(this->list, right.getList());
        case 4:
            return cmp(this->list, right.getList());
        case 5:
            return cmp(this->list, right.getList());            
    }
}
bool Head::operator!= (Head right){
    return !(*this == right);
}
bool Head::operator< (Head right){
    if(this->type != right.getType() || this->type != N){
        cout << "ERROR: operator< compare olny numbers.\n";
        exit(0);
    }
    return this->number < right.getNumber();
}
bool Head::operator> (Head right){
    if(this->type != right.getType() || this->type != N){
        cout << "ERROR: operator> compare olny numbers.\n";
        exit(0);
    }
    return this->number > right.getNumber();
}
bool Head::operator<= (Head right){
    if(this->type != right.getType() || this->type != N){
        cout << "ERROR: operator<= compare olny numbers.\n";
        exit(0);
    }
    return this->number <= right.getNumber();
}
bool Head::operator>= (Head right){
    if(this->type != right.getType() || this->type != N){
        cout << "ERROR: operator>= compare olny numbers.\n";
        exit(0);
    }
    return this->number >= right.getNumber();
}
Head Head::operator+ (Head right){
    if(this->type != right.getType() || this->type != N){
        cout << "ERROR: operator+ sum olny numbers.\n";
        exit(0);
    }
    return *(new Number(this->number + right.getNumber()));
}
Head Head::operator- (Head right){
    if(this->type != right.getType() || this->type != N){
        cout << "ERROR: operator- sub olny numbers.\n";
        exit(0);
    }
    return *(new Number(this->number - right.getNumber()));
}
Head Head::operator* (Head right){
    if(this->type != right.getType() || this->type != N){
        cout << "ERROR: operator* mull olny numbers.\n";
        exit(0);
    }
    return *(new Number(this->number * right.getNumber()));
}
Head Head::operator/ (Head right){
    if(this->type != right.getType() || this->type != N){
        cout << "ERROR: operator/ div olny numbers.\n";
        exit(0);
    }
    return *(new Number(this->number / right.getNumber()));
}
Head Head::operator% (Head right){
    if(this->type != right.getType() || this->type != N){
        cout << "ERROR: operator mod olny numbers.\n";
        exit(0);
    }
    return *(new Number( static_cast<int>(this->number) % static_cast<int>(right.getNumber())));
}

// operators for Head ,int \/
bool Head::operator== (int right){
    if(this->type != N){
        return false;
    }
    return this->number == right;
}
bool Head::operator!= (int right){
    return ! (this->number==right);
}
bool Head::operator< (int right){
    if(this->type != N){
        cout << "ERROR: operator< compare olny numbers.\n";
        exit(0);
    }
    return this->number < right;
}
bool Head::operator> (int right){
    if(this->type != N){
        cout << "ERROR: operator> compare olny numbers.\n";
        exit(0);
    }
    return this->number > right;
}
bool Head::operator<= (int right){
    if(this->type != N){
        cout << "ERROR: operator<= compare olny numbers.\n";
        exit(0);
    }
    return this->number <= right;
}
bool Head::operator>= (int right){
    if(this->type != N){
        cout << "ERROR: operator>= compare olny numbers.\n";
        exit(0);
    }
    return this->number >= right;
}
Head Head::operator+ (int right){
    if(this->type != N){
        cout << "ERROR: operator+ sum olny numbers.\n";
        exit(0);
    }
    return *(new Number(this->number + right));
}
Head Head::operator- (int right){
    if(this->type != N){
        cout << "ERROR: operator- sub olny numbers.\n";
        exit(0);
    }
    return *(new Number(this->number - right));
}
Head Head::operator* (int right){
    if(this->type != N){
        cout << "ERROR: operator* mull olny numbers.\n";
        exit(0);
    }
    return *(new Number(this->number * right));
}
Head Head::operator/ (int right){
    if(this->type != N){
        cout << "ERROR: operator/ div olny numbers.\n";
        exit(0);
    }
    return *(new Number(this->number / right));
}
Head Head::operator% (int right){
    if(this->type != N){
        cout << "ERROR: operator mod olny numbers.\n";
        exit(0);
    }
    return *(new Number( static_cast<int>(this->number) % right));
}

// operators for int,Head \/
bool operator== (int left, Head right){
    if(right.getType() != N){
        return false;
    }
    return right.getNumber() == left;
}
bool operator!= (int left, Head right){
    return ! (right.getNumber() == left);
}
bool operator< (int left, Head right){
    if(right.getType() != N){
        cout << "ERROR: operator< compare olny numbers.\n";
        exit(0);
    }
    return right.getNumber() > left;
}
bool operator> (int left, Head right){
    if(right.getNumber() != N){
        cout << "ERROR: operator> compare olny numbers.\n";
        exit(0);
    }
    return right.getNumber() < left;
}
bool operator<= (int left, Head right){
    if(right.getType() != N){
        cout << "ERROR: operator<= compare olny numbers.\n";
        exit(0);
    }
    return right.getNumber() >= left;
}
bool operator>= (int left, Head right){
    if(right.getType() != N){
        cout << "ERROR: operator>= compare olny numbers.\n";
        exit(0);
    }
    return right.getNumber() <= left;
}
Head operator+ (int left, Head right){
    if(right.getType() != N){
        cout << "ERROR: operator+ sum olny numbers.\n";
        exit(0);
    }
    return *(new Number(right.getNumber() + left));
}
Head operator- (int left, Head right){
    if(right.getType() != N){
        cout << "ERROR: operator- sub olny numbers.\n";
        exit(0);
    }
    return *(new Number(left-right.getNumber()));
}
Head operator* (int left, Head right){
    if(right.getType() != N){
        cout << "ERROR: operator* mull olny numbers.\n";
        exit(0);
    }
    return *(new Number(right.getNumber() * left));
}
Head operator/ (int left, Head right){
    if(right.getType() != N){
        cout << "ERROR: operator/ div olny numbers.\n";
        exit(0);
    }
    return *(new Number(left / right.getNumber()));
}
Head operator% (int left, Head right){
    if(right.getType() != N){
        cout << "ERROR: operator mod olny numbers.\n";
        exit(0);
    }
    return *(new Number( left % static_cast<int>(right.getNumber())));
}