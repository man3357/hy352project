#include "./hy352_gui.h"
#include "functions.h"
#ifdef APPLE
#include <allegro5/allegro.h> //**
#endif
START_PROGRAM
    MAKE turtleMoves = LIST [
                                ARRAY {WORD: "FORWARD", NUMBER: 100},
                                ARRAY {WORD: "RIGHT", NUMBER: 90},
                                ARRAY {WORD: "FORWARD", NUMBER: 100},
                                ARRAY {WORD: "RIGHT", NUMBER: 90},
                                ARRAY {WORD: "FORWARD", NUMBER: 100},
                                ARRAY {WORD: "LEFT", NUMBER: 90},
                                ARRAY {WORD: "FORWARD", NUMBER: 100},
                                ARRAY {WORD: "LEFT", NUMBER: 100}
                            ]
    CENTER
    SETSCREENCOLOR [NUMBER:255,NUMBER:255,NUMBER:255]
    SETPENCOLOR [NUMBER:0,NUMBER:0,NUMBER:0]
    REPEAT 2 TIMES DO
        FOREACH turtleMoves DO
            IF ITEM ({1}, ELEM) == (WORD:"FORWARD") DO
                FORWARD ITEM ({2}, ELEM)
            ELIF ITEM ({1}, ELEM) == (WORD:"BACK") DO
                BACK ITEM ({2}, ELEM)
            ELIF ITEM ({1}, ELEM) == (WORD:"LEFT") DO
                LEFT ITEM ({2}, ELEM)
            ELSE
                RIGHT ITEM ({2}, ELEM)
            END
        END
    END
    CENTER
    RIGHT NUMBER: 90
    FORWARD NUMBER: 150
    REPEAT 360 TIMES DO
        FORWARD NUMBER: 2
        LEFT NUMBER: 1
    END
END_PROGRAM